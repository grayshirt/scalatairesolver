# Scalataire Solver

## What is it?
A solver for the various types of Solitare card games, implemented as an exercise in Scala.

## What's the algorithm?
A simple implementation of [A*](https://en.wikipedia.org/wiki/A*_search_algorithm) employing a priority queue, with 
game states prioritized by their distance from a solution. An easy optimization avoiding repeatedly searching the same 
game state (which can be arrived at via different permutations of the same moves) greatly improves performance.

## License
This code is offered under the MIT License.