package solitaireGames.pyramid

import playingCards.{Deck, Card}

import scala.annotation.tailrec

/**
 * A board for a single state of a game of Pyramid.
 *
 * @param rows the number of rows of cards on this board
 * @param cards the cards comprising this board, including empty spots represented by None
 */
case class Board private(rows: Int, private val cards: IndexedSeq[Option[Card]]) {
  locally {
    val len = cards.length
    val sum = (1 to rows).sum
    require(len == sum, s"expected $sum Cards for $rows-row pyramid; given $len")
  }

  // Internals
  private lazy val exposedCardIndices: IndexedSeq[Int] =
    cards.indices filter isExposed filter (cards(_).isDefined)

  private def isExposed(index: Int): Boolean = {
    if (Board.rowOf(index) == rows - 1) true
    else {
      val firstBlocker = Board.firstBlockerOf(index)
      cards(firstBlocker).isEmpty && cards(firstBlocker + 1).isEmpty
    }
  }

  // Methods for use in calculating neighboring PyramidGameStates
  private def firstExposedKing: Option[Int] =
    exposedCardIndices find (cards(_).get.rank == 13)

  private def matchedPairs: Traversable[(Int, Int)] = {
    @tailrec
    def loop(pairs: Seq[(Int, Int)], remainingIndices: IndexedSeq[Int]): Seq[(Int, Int)] = remainingIndices match {
      case head +: tail =>
        val matches = tail filter (cards(_).get.rank + cards(head).get.rank == 13) map ((head, _))
        loop(pairs ++ matches, tail)
      case _ => pairs
    }
    loop(Seq(), exposedCardIndices)
  }

  private def matchesFor(other: Card): IndexedSeq[Int] =
    exposedCardIndices filter (cards(_).get.rank + other.rank == 13)

  // Public methods returning Boards neighboring this
  /**
   * Find a Board created by removing one exposed king, if any.
   *
   * @return Option containing a copy of this Board without the first exposed King if present
   */
  def boardFromRemovingKing: Option[Board] = firstExposedKing match {
    case None => None
    case Some(i) => Some(Board(rows, cards.updated(i, None)))
  }

  /**
   * Find all Boards created by removing matched pairs of exposed cards, if any.
   *
   * @return a sequence containing any Boards reachable in this manner
   */
  def boardsFromMatchedPairs: Traversable[Board] =
    matchedPairs map { case (i, j) => Board(rows, cards.updated(i, None).updated(j, None)) }

  /**
   * Find all Boards created by matching exposed Cards with the given one.
   *
   * @param other the Card to be matched
   * @return a sequence containing any Boards reachable in this manner
   */
  def boardsFromMatchesFor(other: Card): Traversable[Board] =
    matchesFor(other) map (i => Board(rows, cards.updated(i, None)))

  // Public accessors
  /** Checks if the board is empty. */
  lazy val isEmpty: Boolean = cards.head.isEmpty

  /** The number of cards remaining on the board. */
  val nCards: Int = cards count (o => o.isDefined)

  // Boilerplate
  override def toString: String = {
    val outputRowLength = 4 * rows - 2  // two chars per Card, two chars between each pair, pad to match longest row
    @tailrec
    def loop(output: Seq[String], cardsToStringify: IndexedSeq[Option[Card]], row: Int): String = {
      if (row < 0) output mkString "\n"
      else {
        val lastIndex = cardsToStringify.length - 1
        val lastRowStartIndex = lastIndex - row
        val (leftovers, lastRow) = cardsToStringify splitAt lastRowStartIndex
        val rawRowString = lastRow map (o => o.getOrElse("  ").toString) mkString "  "
        val halfPad = "" padTo ((outputRowLength - rawRowString.length) / 2, ' ')
        val rowOutput = "%s%s%s" format (halfPad, rawRowString, halfPad)
        loop(rowOutput +: output, leftovers, row - 1)
      }
    }
    loop(Seq(), cards, rows - 1)
  }
}

/** Companion providing factory and static methods. */
object Board {
  /**
   * Constructs a Board of the given size by dealing from the given Deck.
   *
   * For use in constructing the initial PyramidGameState.
   *
   * @param rows  the number of rows to deal
   * @param deck  the deck from which to deal
   * @return  the Board dealt
   */
  def fromDeck(rows: Int, deck: Deck): (Deck, Board) = {
    require(rows > 0, "rows must be > 0")
    val nCardsNeeded = (1 to rows).sum
    val (cards, newDeck) = deck.deal(nCardsNeeded)
    (newDeck, Board(rows, cards.toIndexedSeq map Option[Card]))
  }

  // Static methods
  private def firstBlockerOf(index: Int): Int = index + Board.rowOf(index) + 1

  private def rowOf(index: Int): Int = {
    @tailrec
    def loop(row: Int, sum: Int, index: Int): Int = {
      if (sum > index) row - 1
      else loop(row + 1, sum + row + 1, index)
    }
    loop(0, 0, index)
  }
}