package solitaireGames.pyramid

import playingCards.Deck
import solitaireGames.GameState

import scala.math.Ordered.orderingToOrdered

/**
 * A state of a game of Pyramid, including the contents of the board, stock, and discard pile.
 *
 * @param board the pyramid board
 * @param stock the deck of cards available to be dealt
 * @param discard the cards already dealt in this cycle
 * @param stockCycles the number of times the discard can be recycled to the stock
 */
case class PyramidGameState private(
    private val board: Board,
    private val stock: Deck,
    private val discard: Deck,
    private val stockCycles: Int)
    extends GameState[PyramidGameState] {

  // Comparison, etc.
  override val estimatedCostToGoal: Int = board.nCards

  override def isGoal: Boolean = board.isEmpty

  // Finding neighboring states
  override def neighbors: Traversable[PyramidGameState] = neighborFromRemovingKing match {
    // If a king can be removed from this state, optimize by doing that immediately -- treat it as the only neighbor
    case Some(state) => state :: Nil
    case None => neighborsFromMatchingPairs ++ neighborsFromMatchingDiscard ++ neighborsFromMatchingStock ++
      neighborFromDealing ++ neighborFromMatchingStockToDiscard ++ neighborFromRecycling
  }

  private def neighborFromDealing: Traversable[PyramidGameState] = {
    if (!stock.isEmpty) {
      val (card, newStock) = stock.dealTop
      PyramidGameState(board, newStock, discard.addTop(card), stockCycles)
    } :: Nil
    else Nil
  }

  private def neighborsFromMatchingDiscard: Traversable[PyramidGameState] =
    for {c <- discard.peekTopOption.toList; b <- board boardsFromMatchesFor c} yield
      PyramidGameState(b, stock, discard removeTop 1, stockCycles)

  private def neighborsFromMatchingPairs: Traversable[PyramidGameState] =
    for {b <- board.boardsFromMatchedPairs} yield PyramidGameState(b, stock, discard, stockCycles)

  private def neighborsFromMatchingStock: Traversable[PyramidGameState] =
    for {c <- stock.peekTopOption.toList; b <- board boardsFromMatchesFor c} yield
      PyramidGameState(b, stock removeTop 1, discard, stockCycles)

  private def neighborFromMatchingStockToDiscard: Traversable[PyramidGameState] =
    (stock.peekTopOption, discard.peekTopOption) match {
      case (Some(s), Some(d)) if s.rank + d.rank == 13 =>
        PyramidGameState(board, stock removeTop 1, discard removeTop 1, stockCycles) :: Nil
      case _ => Nil
    }

  private def neighborFromRecycling: Traversable[PyramidGameState] =
    if (stockCycles > 0 && stock.isEmpty && !discard.isEmpty)
      PyramidGameState(board, discard.reverse, Deck.empty, stockCycles - 1) :: Nil
    else Nil

  private def neighborFromRemovingKing: Option[PyramidGameState] =
    // Remove kings from top of stock immediately -- never allow to reach discard, so no need to check it
    if (stock.peekTopOption exists (_.rank == 13)) {
      Some(PyramidGameState(board, stock removeTop 1, discard, stockCycles))
    }
    else {
      board.boardFromRemovingKing map (b => PyramidGameState(b, stock, discard, stockCycles))
    }

  // Boilerplate
  override def toString =
    Seq(
      board.toString,
      "Stock (%d, %d cycles): %s".format(stock.length, stockCycles, stock),
      "Discard (%d): %s".format(discard.length, discard)
    ) mkString "\n"
}

object PyramidGameState {
  /**
   * Constructs an initial PyramidGameState of the given size by dealing from the given Deck.
   *
   * @param rows  the number of rows to deal
   * @param deck  the deck from which to deal
   * @return  the PyramidGameState dealt
   */
  def fromDeck(rows: Int, deck: Deck) = Board.fromDeck(rows, deck) match {
    case (stock, board) => PyramidGameState(board, stock, Deck.empty, 2)
  }
}