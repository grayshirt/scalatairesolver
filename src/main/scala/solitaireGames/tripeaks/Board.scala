package solitaireGames.tripeaks

import playingCards.{Deck, Card}

/**
  * A board for a single game state of Tripeaks.
  *
  * @param cards the cards comprising this board, including empty spots represented by None
  */
case class Board private(private val cards: IndexedSeq[Option[Card]]) {
  require(cards.length == Board.NPositions)

  // Internals
  private lazy val exposedCardIndices: IndexedSeq[Int] =
    cards.indices filter isExposed filter (cards(_).isDefined)

  private def isExposed(index: Int): Boolean = {
    if (index > 17) true
    else {
      val firstBlocker = Board.firstBlockerOf(index)
      cards(firstBlocker).isEmpty && cards(firstBlocker + 1).isEmpty
    }
  }

  private def matchesFor(other: Card): Traversable[Int] =
    exposedCardIndices filter (i => Board.areMatched(cards(i).get, other))

  // Public method for use in finding neighboring game states
  /**
    * Find all Boards created by matching exposed Cards with the given one.
    *
    * @param other the Card to be matched
    * @return a sequence containing any tuples of Boards reachable in this manner and the removed Card
    */
  def boardsFromMatchesFor(other: Card): Traversable[(Board, Card)] =
    matchesFor(other) map (i => (Board(cards.updated(i, None)), cards(i).get))

  // Public accessors
  /** Checks if the board is empty. */
  lazy val isEmpty: Boolean = cards.take(3) forall (_.isEmpty)

  /** The number of cards remaining on the board. */
  val nCards: Int = cards count (o => o.isDefined)

  // Boilerplate
  override def toString = {
    val rowOne = "      %s          %s          %s      ".format((cards.take(3) map (o => o getOrElse "  ")):_*)
    val rowTwo = "    %s  %s      %s  %s      %s  %s    ".format((cards.slice(3, 9) map (o => o getOrElse "  ")):_*)
    val rowThree = "  " + ((cards.slice(9, 18) map (o => o getOrElse "  ")) mkString "  ") + "  "
    val rowFour = cards.drop(18) map (o => o getOrElse "  ") mkString "  "
    Seq(rowOne, rowTwo, rowThree, rowFour) mkString "\n"
  }
}

/** Companion providing factories and static methods. */
object Board {
  /**
    * Constructs a Board of the given size by dealing from the given Deck.
    *
    * For use in constructing the initial TripeaksGameState.
    *
    * @param deck  the deck from which to deal
    * @return  the Board dealt
    */
  def fromDeck(deck: Deck): (Deck, Board) = {
    val (cards, newDeck) = deck.deal(NPositions)
    (newDeck, Board(cards.toIndexedSeq map Option[Card]))
  }
  // Statics, etc.
  val NPositions = 28

  private def firstBlockerOf(index: Int): Int = index match {
    case i if i < 3 => 3 + 2 * i
    case i if 3 <= i && i < 9 => i + 6 + ((i - 3) / 2)
    case i => i + 9
  }

  private def areMatched(a: Card, b: Card): Boolean = {
    val diff = Math.abs(a.rank - b.rank)
    diff == 1 || diff == 12
  }
}