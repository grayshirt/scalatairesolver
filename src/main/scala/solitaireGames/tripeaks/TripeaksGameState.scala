package solitaireGames.tripeaks

import playingCards.{Card, Deck}
import solitaireGames.GameState

/**
  * A state of a game of Tripeaks, including the contents of the board, stock, and card to play.
  *
  * @param board the pyramid board
  * @param stock the deck of cards available to be dealt
  * @param topCard the card to be matched next
  */
case class TripeaksGameState private(
    private val board: Board,
    private val stock: Deck,
    private val topCard: Card)
    extends GameState[TripeaksGameState] {

  // Comparison, etc.
  override val estimatedCostToGoal: Int = board.nCards

  override def isGoal: Boolean = board.isEmpty

  // Finding neighboring states
  override def neighbors: Traversable[TripeaksGameState] = neighborsFromMatching ++ neighborFromDealing

  private def neighborsFromMatching: Traversable[TripeaksGameState] =
    for {(b, c) <- board.boardsFromMatchesFor(topCard)} yield TripeaksGameState(b, stock, c)

  private def neighborFromDealing: Traversable[TripeaksGameState] = stock.peekTopOption match {
    case None => Nil
    case Some(c) => TripeaksGameState(board, stock removeTop 1, c) :: Nil
  }

  // Boilerplate
  override def toString =
    Seq(
      board.toString,
      "Stock (%d): %s".format(stock.length, stock),
      "Card to play: %s" format topCard
    ) mkString "\n"
}

object TripeaksGameState {
  /**
    * Constructs an initial TripeaksGameState by dealing from the given Deck.
    *
    * @param deck  the deck from which to deal
    * @return  the TripeaksGameState dealt
    */
  def fromDeck(deck: Deck) = Board.fromDeck(deck) match {
    case (leftovers, board) => leftovers.dealTop match {
      case (topCard, stock) => TripeaksGameState(board, stock, topCard)
    }
  }
}