package solitaireGames

/**
 * Interface defining members common to game states.
 */
abstract class GameState[A <: GameState[A]] {
  /** Checks if this game state is the goal state. */
  def isGoal: Boolean
  /** Finds all states that can be reached by one legal move from this state. */
  def neighbors: Traversable[A]
  /** Returns an estimate of the distance from this state to the solution. */
  val estimatedCostToGoal: Int
}