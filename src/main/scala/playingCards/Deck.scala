package playingCards

import scala.annotation.tailrec
import scala.util.Random

/**
 * A deck of [[playingCards.Card]]s.
 *
 * Should be constructed from companion object's factories.
 */
case class Deck private (private val cards: IndexedSeq[Card]) {
  /** Returns a copy of this deck with the given card added to the top. */
  def addTop(card: Card): Deck = Deck(card +: cards)

  /** Returns cards from the top of this deck and a new deck without those cards. */
  def deal(n: Int): (Seq[Card], Deck) = {
    require(n >= 0, "n must be > 0")
    require(length >= n, s"$n cards requested from Deck containing only $length")
    val (dealt, remaining) = cards splitAt n
    (dealt, Deck(remaining))
  }

  /** Returns the card and deck created by dealing the top card if this deck is not empty. */
  def dealTop: (Card, Deck) = {
    require(!isEmpty, "deck is empty")
    val (cardSeq, deck) = deal(1)
    (cardSeq.head, deck)
  }

  /** Checks if this deck is empty. */
  def isEmpty: Boolean = cards.isEmpty

  /** Returns the number of cards in this deck. */
  def length: Int = cards.length

  /** Returns the top card from this deck if non-empty. */
  def peekTopOption: Option[Card] = cards.headOption

  /** Returns a copy of this deck with the given number of cards removed from the top. */
  def removeTop(n: Int): Deck = {
    require(n >= 0, "n must be > 0")
    require(n <= length, s"cannot remove $n cards from deck of length $length")
    Deck(cards drop n)
  }
  
  /** Returns a reversed copy of this deck. */
  def reverse: Deck = new Deck(cards.reverse)

  /** Returns a shuffled copy of this deck. */
  def shuffle: Deck = new Deck(Random.shuffle(cards))

  override def toString: String = cards mkString ", "
}

/** Factory for [[playingCards.Deck]] instances. */
object Deck {

  private val stringDelimiter = ","

  /** Returns an empty deck. */
  def empty: Deck = Deck(IndexedSeq())

  /**
    * Returns Deck specified by the given string.
    *
    * @param chars comma-delimited sequence of two-character [[playingCards.Card]] specifiers -- "As, 2h, 3d";
    *              whitespace is ignored
    * @return Deck
    */
  def fromCharSequence(chars: CharSequence): Deck = {
    val cardChars = chars.toString split stringDelimiter
    val cards = cardChars map (s => Card.fromChars(s.trim))
    Deck(cards)
  }

  /** Returns a standard 52-card deck unshuffled. */
  def unshuffled: Deck = Deck(for {s <- "cdhs".toIndexedSeq; r <- 1 to 13} yield Card(r, s))
  /** Returns a standard 52-card deck shuffled. */
  def shuffled: Deck = unshuffled.shuffle

  def main(args: Array[String]) {
    val deckString = "Qd, Ac, 7h, Ts, Jd, Kh"
    val deck = Deck.fromCharSequence(deckString)
    println(deck)
  }
}