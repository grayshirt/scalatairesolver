package playingCards

/**
 * A standard playing card.
 *
 * @constructor create a new Card of the given rank and suit
 * @param rank the rank of the card; 1 (ace) to 13 (king)
 * @param suit the suit of the card represented by its initial -- 'c'lubs, 'd'iamonds, 'h'earts, 's'pades
 */
case class Card(rank: Int, suit: Char) {
  require(0 < rank && rank < 14, s"given rank $rank invalid; 1 to 13 permitted")
  require("cdhs".contains(suit), s"given suit '$suit' invalid; c, d, s or h permitted")

  override def toString: String = {
    val rankString: String = rank match {
      case 13 => "K"
      case 12 => "Q"
      case 11 => "J"
      case 10 => "T"
      case 1 => "A"
      case _ => rank.toString
    }
    rankString concat suit.toString
  }
}

/** Factory for [[playingCards.Card]] instances. */
object Card {
  /**
    * Returns card specified by the given two characters.
    *
    * @param chars a two-character sequence in the same format output by Card.toString -- "Ac", "Th", "5s"
    * @return Card
    */
  def fromChars(chars: CharSequence): Card = {
    require(chars.length() == 2, s"$chars invalid initializer for Card")
    val rank = getRank(chars.charAt(0))
    val suit = chars.charAt(1)
    Card(rank, suit)
  }

  private def getRank(c: Char): Int = c match {
    case 'K' => 13
    case 'Q' => 12
    case 'J' => 11
    case 'T' => 10
    case 'A' => 1
    case d => Integer.parseInt(d.toString)
  }
}