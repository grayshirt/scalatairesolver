package solver

// TODO: Refactor with SortedSet (need workaround for SortedSet treating ordering as identity)

import solitaireGames.GameState

import scala.annotation.tailrec
import scala.collection.mutable
import scala.util.control.Breaks._

/**
 * Finds a solution from the given initial game state.
 *
 * @param initialState the game state from which to begin the search for a solution
 * @tparam A the GameState implementation type representing the game to be solved
 */
class Solver[A <: GameState[A]](initialState: A) {

  private case class Node(state: A, previous: Option[Node], moves: Int) extends Ordered[Node] {

    private val totalCostEstimate: Int = state.estimatedCostToGoal + moves

    def compare(that: Node): Int = -(totalCostEstimate compare that.totalCostEstimate) // invert for max-oriented PQ
  }

  private var solutionStates: Option[List[A]] = None

  private[this] var _timeInSeconds: Double = 0.0

  def timeInSeconds: Double = _timeInSeconds

  // Compute solution
  locally {
    val pq = new mutable.PriorityQueue[Node]()
    val visitedStateHashes = new mutable.HashSet[Int]()
    val startTime = System.nanoTime
    pq.enqueue(Node(initialState, None, 0))
    var currentNode = pq.dequeue()
    breakable {
      while (!currentNode.state.isGoal) {
        val stateHash = currentNode.state.hashCode
        if (!visitedStateHashes(stateHash)) {
          visitedStateHashes.add(stateHash)
          val neighbors = currentNode.state.neighbors
          neighbors foreach (s => pq.enqueue(Node(s, Some(currentNode), currentNode.moves + 1)))
        }
        if (pq.nonEmpty) {
          currentNode = pq.dequeue()
        }
        else break()
      }
    }
    val endTime = System.nanoTime
    _timeInSeconds = (endTime - startTime) * 1e-9
    if (currentNode.state.isGoal) {
      @tailrec
      def loop(node: Node, acc: List[A]): List[A] = node.previous match {
        case Some(parent) => loop(parent, node.state :: acc)
        case None => node.state :: acc
      }
      solutionStates = Some(loop(currentNode, List[A]()))
    }
  }

  /** The path states from the initial game state to the solution, if any found. */
  def solution: Option[List[A]] = solutionStates
}