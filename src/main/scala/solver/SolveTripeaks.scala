package solver

import playingCards.Deck
import solitaireGames.tripeaks.TripeaksGameState

/** Provides script to solve a game of Tripeaks. */
object SolveTripeaks extends App {
  if (args.length > 1) {
    println("Deck argument, if given, is a single string; enclose in quotes or omit whitespace")
    System.exit(1)
  }
  val deck = args match {
    case Array(deckString) => Deck.fromCharSequence(deckString)
    case Array() => Deck.shuffled
  }
  println("Deck:\n" + deck)
  val initialState = TripeaksGameState.fromDeck(deck)
  val solver = new Solver(initialState)
  solver.solution match {
    case None => println("Unsolved!")
    case Some(states) => println("Won!"); states.zipWithIndex foreach { case (s, i) => println("%d\n%s".format(i, s))
    }
  }
  println("Completed in %.2f seconds".format(solver.timeInSeconds))
}
